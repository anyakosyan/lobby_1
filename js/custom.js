$(function () {
    $('.language').on('click',function (e) {
        e.stopPropagation()
        $(this).find('.language_list').toggleClass('d-none')
        $(this).find('.select-icon').toggleClass('rotate')
    })
    $('body').on('click',function () {
        $(this).find('.language_list').addClass('d-none')
        $(this).find('.select-icon').removeClass('rotate')
    })
    $('.language__item').on('click',function () {
        $('.language__item').removeClass('selected')
        $(this).addClass('selected')
        const dataID = $(this).attr('data-id');
        const language = $(this).text();
        $(this).parents('.language').find('.language__text ').text(language)
        if(dataID == 'arabic'){
            $('body').addClass('arabic')
        }else {
            $('body').removeClass('arabic')
        }
    })
    var selectedClass = "";
    $('.game-tabs__item').click(function(){
        $('.game-tabs__item').removeClass('active');
        $(this).addClass('active');
        selectedClass = $(this).attr("data-filter");
        $(".game-filter .game-item").hide();
        $(".game-filter .game-item." + selectedClass).show();
    });

    $('.menu-icon').click(function () {
        $(this).toggleClass('active')
        $(this).parents('body').find('.header').slideToggle(700)
    })

    $('.mobile-header .language').click(function () {
        $(this).parents('body').find('.header').css('display','none');
        $(this).parents('body').find('.menu-icon').removeClass('active');
    })

    $('.header_right-menu button').click(function () {
        if($(window).width() <= 767) {
            $(this).parents('.header').hide()
            $(this).parents('body').find('.menu-icon ').removeClass('active')
        }
    })

})